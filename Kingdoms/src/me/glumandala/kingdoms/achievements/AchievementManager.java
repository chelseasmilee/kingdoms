package me.glumandala.kingdoms.achievements;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import me.glumandala.kingdoms.util.ItemMaker;

public class AchievementManager extends JavaPlugin {
	
	public static void giveAchievementViewer(Player p){
		ItemMaker.createItem(new ItemStack(Material.NETHER_STAR, 1), ChatColor.AQUA+"Achievements", new String[] {"Schau deine Erfolge an!"});
	}
}
