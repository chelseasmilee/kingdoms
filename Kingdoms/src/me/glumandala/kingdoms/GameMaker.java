package me.glumandala.kingdoms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import me.glumandala.kingdoms.achievements.AchievementManager;
import me.glumandala.kingdoms.kits.Bogenschuetze;
import me.glumandala.kingdoms.kits.Kerkermeister;
import me.glumandala.kingdoms.kits.Koenig;
import me.glumandala.kingdoms.kits.Ritter;
import me.glumandala.kingdoms.util.Kits;
import me.glumandala.kingdoms.util.ScoreboardManager;
import me.glumandala.kingdoms.util.Team;

public class GameMaker extends JavaPlugin  {
	public static HashMap<Player, Team> players = new HashMap<Player, Team>();
	public static HashMap<Kits, Team> usedKits = new HashMap<Kits, Team>();
	public static HashMap<Player, Team> kings = new HashMap<Player, Team>();
	public static boolean chooser = false;
	public static boolean inGame = false;
	public static boolean freeze = false;
	public static int redCounter = 0;
	public static int blueCounter = 0;
	
	//TODO: Make changeable in config
	public static int MIN_PLAYERS = 2;
	public static String WORLD = Kingdoms.plugin.getConfig().getString("MAIN_WORLD");
	
	public static void addPlayer(Player p){
		p.getInventory().clear();
		AchievementManager.giveAchievementViewer(p);
		Team t;
		if(chooser == false){ t = Team.RED; chooser = true; } else { t = Team.BLUE; chooser = false; }
		
		if(!players.containsKey(p)){
			players.put(p, t);
			p.sendMessage(ChatColor.GREEN+"Du nimmst nun am Spiel teil.");
			p.teleport(new Location(Bukkit.getWorld(GameMaker.WORLD), Kingdoms.plugin.getConfig().getDouble("spawn_loc_lobby_x"), Kingdoms.plugin.getConfig().getDouble("spawn_loc_lobby_y"), Kingdoms.plugin.getConfig().getDouble("spawn_loc_lobby_z")));
			
			if(players.size() == 1){
				Bukkit.getServer().getScheduler().runTaskTimerAsynchronously(Kingdoms.plugin, new Runnable() {
					public void run() {
						if(players.size() != MIN_PLAYERS){
							for(Player p : players.keySet()){
								p.sendMessage(ChatColor.GOLD+"> Es fehlen noch "+ChatColor.BOLD+(MIN_PLAYERS-players.size())+ChatColor.GOLD+" Spieler.");
							}
						} else {
							freeze = true;
							ScoreboardManager.initScoreboard();
							ArrayList<Player> red = new ArrayList<Player>();
							ArrayList<Player> blue = new ArrayList<Player>();
							
							for(Player p : players.keySet()){
								if(players.get(p).equals(Team.BLUE)){ blueCounter++; blue.add(p); p.sendMessage(ChatColor.GRAY+"Du bist im "+ChatColor.BLUE+"Team Blau"+ChatColor.GRAY+".");}
								if(players.get(p).equals(Team.RED)){ redCounter++; red.add(p); p.sendMessage(ChatColor.GRAY+"Du bist im "+ChatColor.RED+"Team Rot"+ChatColor.GRAY+".");}
								
								p.sendMessage(ChatColor.RED+"> Das Spiel beginnt in 3 Sekunden!");
								String kit = getKit(p);
								String team = players.get(p).name().toLowerCase();
								if(team == "red") team = "rot"; else team = "blau";
								
								System.out.println(Kingdoms.plugin.getConfig().getDouble("spawn_loc_"+kit+"_"+team+"_x"));
								System.out.println(kit);
								p.teleport(new Location(Bukkit.getWorld(WORLD),
										Kingdoms.plugin.getConfig().getDouble("spawn_loc_"+kit+"_"+team+"_x"),
										Kingdoms.plugin.getConfig().getDouble("spawn_loc_"+kit+"_"+team+"_y"),
										Kingdoms.plugin.getConfig().getDouble("spawn_loc_"+kit+"_"+team+"_z")));
							}
							
							Player redK = null, blueK = null;
							int randomKingRed = (int)((Math.random()) * red.size() + 1);
							int randomKingBlue = (int)((Math.random()) * blue.size() + 1);
							Iterator<Player> it = red.iterator();
							Iterator<Player> it2 = blue.iterator();
							
							for(int i = 1; i <= randomKingRed; i++) redK = it.next();
							for(int i = 1; i <= randomKingBlue; i++) blueK = it2.next();
							
							kings.put(redK, Team.RED);
							kings.put(blueK, Team.BLUE);
							ScoreboardManager.setDisplaynames();
							
							for(Player p : players.keySet()){
								p.sendMessage(ChatColor.RED+"ROT: "+redK.getName());
								p.sendMessage(ChatColor.BLUE+"BLAU: "+blueK.getName());
							}
							
							inGame = true;
							Bukkit.getScheduler().cancelTasks(Kingdoms.plugin);
							Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Kingdoms.plugin, new Runnable() {
								public void run() {
									freeze = false;
								}
							}, 60L);
						}
					}
				}, 30L, 200L);
			}
		} else {
			if(!inGame)
			 p.sendMessage(Kingdoms.prefix+ChatColor.RED+"Du bist bereits in der Warteschlange."); else
		     p.sendMessage(Kingdoms.prefix+ChatColor.RED+"Du spielst bereits Kingdoms.");
		}
	}
	
	public static String getKit(Player p){
		String kit = "";
		int randomKitNumber = (int)((Math.random()) * 7 + 1);
		if(randomKitNumber == 1){
			if(usedKits.get(p) != players.get(p)){
				Ritter.giveKit(p, 1);
				p.sendMessage(Kingdoms.prefix+ChatColor.GRAY+"Du hast das Ritter-Kit");
				usedKits.put(Kits.RITTER, GameMaker.players.get(p));
				kit = "ritter";
			} else {
				getKit(p);
			}
		}
		
		if(randomKitNumber == 2){
			if(usedKits.get(p) != players.get(p)){
				Koenig.giveKit(p, 1);
				p.sendMessage(Kingdoms.prefix+ChatColor.GRAY+"Du hast das K�nigs-Kit");
				usedKits.put(Kits.KOENIG, GameMaker.players.get(p));
				kit = "k�nig";
			} else {
				getKit(p);
			}
		}
		
		if(randomKitNumber == 3){
			if(usedKits.get(p) != players.get(p)){
				Kerkermeister.giveKit(p, 1);
				p.sendMessage(Kingdoms.prefix+ChatColor.GRAY+"Du hast das Kekermeister-Kit");
				usedKits.put(Kits.KERKERMEISTER, GameMaker.players.get(p));
				kit = "kerkermeister";
			} else {
				getKit(p);
			}
		}
		
		if(randomKitNumber == 4){
			if(usedKits.get(p) != players.get(p)){
				Bogenschuetze.giveKit(p, 1);
				p.sendMessage(Kingdoms.prefix+ChatColor.GRAY+"Du hast das Bogensch�tzen-Kit");
				usedKits.put(Kits.BOEGENSCHUETZE, GameMaker.players.get(p));
				kit = "bogensch�tze";
			} else {
				getKit(p);
			}
		}
		
		if(randomKitNumber == 5){
			if(usedKits.get(p) != players.get(p)){
				Bogenschuetze.giveKit(p, 1);
				p.sendMessage(Kingdoms.prefix+ChatColor.GRAY+"Du hast das Heiler-Kit");
				usedKits.put(Kits.HEILER, GameMaker.players.get(p));
				kit = "heiler";
			} else {
				getKit(p);
			}
		}
		
		return kit;
	}
	
	public static void removePlayer(Player p){
		players.remove(p);
		if(players.containsKey(p)) removePlayer(p);
	}
	
	public static Integer getPlayers(Team t){
		int counter = 0;
		if(t.equals(Team.RED)){
			for(Team t2 : players.values()){
				if(t2.equals(Team.RED)) counter++;
			}
		} else if(t.equals(Team.BLUE)) {
			for(Team t2 : players.values()){
				if(t2.equals(Team.BLUE)) counter++;
			}
		}
		
		return counter;
	}
	
	public static void resetGame(){
		players.clear();
		inGame = false;
		for(Player p : kings.keySet()){
			p.removeMetadata("kinglives", Kingdoms.plugin);
			p.setGameMode(GameMode.SURVIVAL);
		}
		kings.clear();
		ScoreboardManager.resetScoreboard();
		redCounter = 0;
		blueCounter = 0;
	}
}
