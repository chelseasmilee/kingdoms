package me.glumandala.kingdoms.util;

public enum Team {
    RED("rot"), BLUE("blau"), NONE("none");
	 
    final String type;

    private Team(String type) {
        this.type = type;
    }
}
