package me.glumandala.kingdoms.util;

public enum Kits {
	RITTER("Ritter"), HEXE("Hexe"), KERKERMEISTER("Kerkermeister"), FRONTENKAEMPFER("Frontenkämpfer"), 
	BOEGENSCHUETZE("Bogenschuetze"), HEILER("Heiler"), KOENIG("König");
	 
    final String type;

    private Kits(String type) {
        this.type = type;
    }
}
