package me.glumandala.kingdoms.util;

import java.util.Arrays;
import java.util.Map;

import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

public class ItemMaker extends JavaPlugin {

	public static ItemStack enchantItem(ItemStack is, Map<Enchantment, Integer> en){
		is.addEnchantments(en);
		
		return is;
	}
	
    public static ItemStack createItem(ItemStack item, String name, String[] lore) {
        ItemMeta im = item.getItemMeta();
        im.setDisplayName(name);
        im.setLore(Arrays.asList(lore));
        item.setItemMeta(im);
        return item;
    }
}
