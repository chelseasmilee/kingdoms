package me.glumandala.kingdoms.util;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;

import me.glumandala.kingdoms.GameMaker;

public class ScoreboardManager extends JavaPlugin {
	private static org.bukkit.scoreboard.ScoreboardManager manager = Bukkit.getScoreboardManager();
	private static Scoreboard board = manager.getNewScoreboard();
	private static org.bukkit.scoreboard.Team blue = board.registerNewTeam("kingdomsBlue");
	private static org.bukkit.scoreboard.Team red = board.registerNewTeam("kingdomsRed");
	private static Objective objective = board.registerNewObjective("Kingdoms", "dummy");
	
	public static void initScoreboard(){
		for(Player p : GameMaker.players.keySet()){
			String teamString = GameMaker.players.get(p).name();
			if(teamString.equals("BLUE")){
				if(GameMaker.kings.containsKey(p))
					teamString = ChatColor.BLUE+"K�nig | "+ChatColor.RESET; else 
					teamString = ChatColor.BLUE+"Spieler | "+ChatColor.RESET;
				blue.addEntry(p.getName());
				blue.setPrefix(teamString);
				blue.setDisplayName(teamString+p.getName());
			} else {
				red.addEntry(p.getName());
				red.setDisplayName(teamString+p.getName());
			}
			
			blue.setAllowFriendlyFire(false);
			red.setAllowFriendlyFire(false);
			objective.setDisplaySlot(DisplaySlot.SIDEBAR);
			updateScoreboard(3, 3);
			p.setScoreboard(board);
		}
	}
	
	public static void setDisplaynames(){
		for(Player p : GameMaker.players.keySet()){
			String teamString = GameMaker.players.get(p).name();
				if(GameMaker.players.get(p).equals(Team.BLUE)){
					if(GameMaker.kings.containsKey(p))
						teamString = ChatColor.BLUE+"K�nig | "+ChatColor.RESET; else 
						teamString = ChatColor.BLUE+"Spieler | "+ChatColor.RESET;
						blue.setPrefix(teamString);
				} else {
					if(GameMaker.kings.containsKey(p))
						teamString = ChatColor.RED+"K�nig | "+ChatColor.RESET; else 
						teamString = ChatColor.RED+"Spieler | "+ChatColor.RESET;
						red.setPrefix(teamString);
				}
		}
	}
	
	public static void updateScoreboard(int livesRed, int livesBlue){
			if(livesRed != 5){
			    Score score = objective.getScore(ChatColor.RED+"Team Rot:");
			    score.setScore(livesRed);
			}
			if(livesBlue != 5){
			    Score score2 = objective.getScore(ChatColor.BLUE+"Team Blau:");
			    score2.setScore(livesBlue);
			}
	}
	
	public static void resetScoreboard(){
		//TODO Scoreboard is still there (bug)
		for(Player p : GameMaker.players.keySet()){
			board.resetScores(ChatColor.RED+"Team Rot:");
			board.resetScores(ChatColor.BLUE+"Team Blau:");
			p.setScoreboard(manager.getNewScoreboard());
			objective.unregister();
			try {
				blue.removeEntry(p.getName());
				red.removeEntry(p.getName()); 
			} catch(NullPointerException ex){}
		}
	}
	
	public static int getBlueLives(){
		return objective.getScore(ChatColor.BLUE+"Team Blau:").getScore();
	}
	
	public static int getRedLives(){
		return objective.getScore(ChatColor.RED+"Team Rot:").getScore();
	}
}
