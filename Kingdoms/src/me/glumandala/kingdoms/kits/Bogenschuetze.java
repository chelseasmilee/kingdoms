package me.glumandala.kingdoms.kits;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.glumandala.kingdoms.util.ItemMaker;

public class Bogenschuetze {

	@SuppressWarnings("deprecation")
	public static void giveKit(Player p, int level){
		Map<Enchantment, Integer> enchantments = new HashMap<Enchantment, Integer>();
		Map<Enchantment, Integer> bow = new HashMap<Enchantment, Integer>();
		 
		if(level >= 1){
			enchantments.put(Enchantment.DURABILITY, 3);
		}
		if(level == 2){
			enchantments.put(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
			bow.put(Enchantment.ARROW_DAMAGE, 1);
		}
		if(level == 3){
			enchantments.put(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
			bow.put(Enchantment.ARROW_DAMAGE, 1);
			p.getInventory().addItem(new ItemStack(Material.ARROW, 8));
		}
		if(level == 4){
			enchantments.put(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
			bow.put(Enchantment.ARROW_DAMAGE, 1);
			p.getInventory().addItem(new ItemStack(Material.ARROW, 8));
		}
		if(level == 5){
			enchantments.put(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
			bow.put(Enchantment.ARROW_DAMAGE, 2);
			p.getInventory().addItem(new ItemStack(Material.ARROW, 8));
		}
		
		p.getInventory().setHelmet(ItemMaker.enchantItem(new ItemStack(Material.LEATHER_HELMET, 1, DyeColor.GREEN.getDyeData()), enchantments));
		p.getInventory().setChestplate(ItemMaker.enchantItem(new ItemStack(Material.CHAINMAIL_CHESTPLATE, 1), enchantments));
		p.getInventory().setLeggings(ItemMaker.enchantItem(new ItemStack(Material.LEATHER_LEGGINGS, 1, DyeColor.GREEN.getDyeData()), enchantments));
		p.getInventory().setBoots(ItemMaker.enchantItem(new ItemStack(Material.LEATHER_BOOTS, 1, DyeColor.GREEN.getDyeData()), enchantments));
		
		p.getInventory().addItem(ItemMaker.enchantItem(new ItemStack(Material.BOW, 1), bow));
		p.getInventory().addItem(new ItemStack(Material.ARROW, 16));
	}
}
