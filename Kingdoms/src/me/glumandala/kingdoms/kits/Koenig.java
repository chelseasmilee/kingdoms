package me.glumandala.kingdoms.kits;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.glumandala.kingdoms.util.ItemMaker;

public class Koenig {
	
	@SuppressWarnings("deprecation")
	public static void giveKit(Player p, int level){
		Map<Enchantment, Integer> enchantments = new HashMap<Enchantment, Integer>();
		Map<Enchantment, Integer> sword = new HashMap<Enchantment, Integer>();
		
		if(level >= 1){
			enchantments.put(Enchantment.DURABILITY, 3);
		}
		if(level >= 2){
			enchantments.put(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
		}
		if(level == 3){
			 sword.put(Enchantment.DAMAGE_ALL, 1);
		}
		if(level >= 4){
			 sword.put(Enchantment.DAMAGE_ALL, 2);
		}
		if(level == 5){
			enchantments.put(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
		}
		 
		p.getInventory().setHelmet(ItemMaker.enchantItem(new ItemStack(Material.GOLD_HELMET, 1, DyeColor.WHITE.getData()), enchantments));
		p.getInventory().setChestplate(ItemMaker.enchantItem(new ItemStack(Material.GOLD_CHESTPLATE, 1), enchantments));
		p.getInventory().setLeggings(ItemMaker.enchantItem(new ItemStack(Material.GOLD_LEGGINGS, 1), enchantments));
		p.getInventory().setBoots(ItemMaker.enchantItem(new ItemStack(Material.GOLD_BOOTS, 1, DyeColor.WHITE.getData()), enchantments));
		
		p.getInventory().addItem(ItemMaker.enchantItem(new ItemStack(Material.IRON_SWORD, 1), sword));
	}
}
