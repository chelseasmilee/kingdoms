package me.glumandala.kingdoms.kits;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionType;

import me.glumandala.kingdoms.util.ItemMaker;

public class Heiler {

	@SuppressWarnings("deprecation")
	public static void giveKit(Player p, int level){
		Map<Enchantment, Integer> enchantments = new HashMap<Enchantment, Integer>();
		Map<Enchantment, Integer> sword = new HashMap<Enchantment, Integer>();
		 
		if(level >= 1){
			enchantments.put(Enchantment.DURABILITY, 3);
		}
		if(level == 2){ enchantments.put(Enchantment.PROTECTION_ENVIRONMENTAL, 1); }
		if(level >= 2){
			sword.put(Enchantment.DAMAGE_ALL, 1);
			p.getInventory().addItem(ItemMaker.createItem(new ItemStack(Material.APPLE, 1), ChatColor.RED+"Rote Binde", new String[] {ChatColor.GRAY+"Heilt dich vollständig."}));
		}
		if(level >= 3){
			p.getInventory().addItem(ItemMaker.createItem(new ItemStack(Material.GOLDEN_APPLE, 1), ChatColor.GOLD+"Schicksalsapfel", new String[] {ChatColor.GRAY+"- Heilt den Benutzer 15 Sekunden", ChatColor.GRAY+"- Schadet dem Gegner 5 Sekunden"}));
		}
		if(level >= 4){
			enchantments.put(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
		}
		if(level == 5){
			p.getInventory().addItem(ItemMaker.createItem(new ItemStack(Material.GOLDEN_APPLE, 1), ChatColor.GOLD+"Schicksalsapfel", new String[] {ChatColor.GRAY+"- Heilt den Benutzer 15 Sekunden", ChatColor.GRAY+"- Schadet dem Gegner 5 Sekunden"}));
			Potion potion = new Potion(PotionType.INSTANT_HEAL, 2, true, true);
			ItemStack potionstack = potion.toItemStack(2);
			p.getInventory().addItem(potionstack);
		} else {
			Potion potion = new Potion(PotionType.INSTANT_HEAL, 1, true, true);
			ItemStack potionstack = potion.toItemStack(2);
			p.getInventory().addItem(potionstack);
		}
		
		p.getInventory().setHelmet(ItemMaker.enchantItem(new ItemStack(Material.LEATHER_HELMET, 1, DyeColor.WHITE.getDyeData()), enchantments));
		p.getInventory().setChestplate(ItemMaker.enchantItem(new ItemStack(Material.LEATHER_CHESTPLATE, 1, DyeColor.RED.getDyeData()), enchantments));
		p.getInventory().setLeggings(ItemMaker.enchantItem(new ItemStack(Material.LEATHER_LEGGINGS, 1, DyeColor.WHITE.getDyeData()), enchantments));
		p.getInventory().setBoots(ItemMaker.enchantItem(new ItemStack(Material.LEATHER_BOOTS, 1, DyeColor.WHITE.getDyeData()), enchantments));
		
		p.getInventory().addItem(ItemMaker.enchantItem(new ItemStack(Material.STONE_SWORD, 1), sword));
		p.getInventory().addItem(ItemMaker.createItem(new ItemStack(Material.APPLE, 1), ChatColor.RED+"Rote Binde", new String[] {ChatColor.GRAY+"- Heilt dich vollständig"}));
		//TODO Rote Binde
		//TODO Schicksalsapfel
	}
}
