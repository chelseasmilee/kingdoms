package me.glumandala.kingdoms.kits;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.glumandala.kingdoms.util.ItemMaker;

public class Kerkermeister {
	
	public static void giveKit(Player p, int level){
		Map<Enchantment, Integer> enchantments = new HashMap<Enchantment, Integer>();
		 
		if(level >= 1){
			enchantments.put(Enchantment.DURABILITY, 3);
		}
		if(level == 2){
			enchantments.put(Enchantment.PROTECTION_ENVIRONMENTAL, 1);		
		}
		if(level == 3){
			enchantments.put(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
		}
		if(level >= 4){
			enchantments.put(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
		}
		if(level == 5){
			enchantments.put(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
		}
		 
		p.getInventory().setChestplate(ItemMaker.enchantItem(new ItemStack(Material.IRON_CHESTPLATE, 1), enchantments));
		p.getInventory().setLeggings(ItemMaker.enchantItem(new ItemStack(Material.CHAINMAIL_LEGGINGS, 1), enchantments));
		p.getInventory().setBoots(ItemMaker.enchantItem(new ItemStack(Material.IRON_BOOTS, 1), enchantments));
		
		ItemStack mstern = ItemMaker.createItem(new ItemStack(Material.FISHING_ROD, 1), ChatColor.AQUA+"Morgenstern", new String[] {""});
		p.getInventory().addItem(mstern);
		p.getInventory().addItem(new ItemStack(Material.STONE_SWORD, 1));
		//TODO Morgenstern
	}
}
