package me.glumandala.kingdoms;

import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import me.glumandala.kingdoms.commands.join;
import me.glumandala.kingdoms.commands.leave;
import me.glumandala.kingdoms.commands.setsqlpw;
import me.glumandala.kingdoms.commands.setworld;
import me.glumandala.kingdoms.commands.spawnpoint;
import me.glumandala.kingdoms.events.DeathEvent;
import me.glumandala.kingdoms.events.MoveEvent;

public class Kingdoms extends JavaPlugin {
	private Logger log = Logger.getLogger("Minecraft");
	private PluginDescriptionFile pdf = this.getDescription();
	private static PluginManager pm = Bukkit.getServer().getPluginManager();
	public static Kingdoms plugin;
	public static String prefix = ChatColor.GRAY+"["+ChatColor.GOLD+"Kingdoms"+ChatColor.GRAY+"] ";
	
	public void onEnable() {
		plugin = this;
		
		loadConfiguration();
		registerEvents();
		registerCommands();
		
	    log.info(pdf.getName() + " Version " + pdf.getVersion() + " wurde geladen!");
	}
	
	public void onDisable() {
		log.info(pdf.getName()+" wurde heruntergefahren.");
	}

	private void registerEvents(){
		pm.registerEvents(new DeathEvent(), this);
		pm.registerEvents(new MoveEvent(), this);
	}
	
	private void registerCommands(){
		getCommand("join").setExecutor(new join(this));
		getCommand("leave").setExecutor(new leave(this));
		getCommand("spawnpoint").setExecutor(new spawnpoint(this));
		getCommand("setsqlpw").setExecutor(new setsqlpw(this));
		getCommand("setworld").setExecutor(new setworld(this));
	}
	
	private void loadConfiguration(){
		//getConfig().addDefault("EVENT_COUNTDOWN", 10);
		getConfig().options().copyDefaults(true);
		saveConfig();
	}
}
