package me.glumandala.kingdoms.events;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;

import me.glumandala.kingdoms.GameMaker;
import me.glumandala.kingdoms.Kingdoms;
import me.glumandala.kingdoms.util.ScoreboardManager;
import me.glumandala.kingdoms.util.Team;

public class DeathEvent implements Listener {
	private Integer lives = 2;
	
	@EventHandler
    public void onPlayerDeath(PlayerDeathEvent event)  {
        if(GameMaker.inGame){
        	Player pl = event.getEntity();
        	event.setDeathMessage(ChatColor.GRAY+pl.getDisplayName()+" ist gestorben.");
        	
        	if(GameMaker.kings.containsKey(pl)){
        		for(Player p : GameMaker.players.keySet()) p.playSound(p.getLocation(), Sound.valueOf("ENTITY_IRONGOLEM_DEATH"), 3.0F, 0.533F);
        		if(pl.hasMetadata("kinglives")){
        			for(MetadataValue s : pl.getMetadata("kinglives")){
        				lives = s.asInt();
        			}
        			
        			if(GameMaker.kings.get(pl).equals(Team.RED))ScoreboardManager.updateScoreboard(lives, 5); else
        				ScoreboardManager.updateScoreboard(5, lives);
        			
        			if(lives == 0){
        				String winnerTeam = GameMaker.kings.get(pl).name();
        				if(winnerTeam.equals("BLUE")){ GameMaker.redCounter--; winnerTeam = "Blau"; } else { GameMaker.blueCounter--; winnerTeam = "Rot"; }
        				for(Player p : GameMaker.players.keySet()) p.sendMessage(Kingdoms.prefix+ChatColor.GREEN+"Team "+winnerTeam+" hat keinen K�nig mehr!\n"+ChatColor.GRAY+"Die Spieler aus Team "+winnerTeam+" k�nnen nicht mehr respawnen!");
        			} else {
        				pl.setMetadata("kinglives", new FixedMetadataValue(Kingdoms.plugin, lives-1));
        				String teamName = GameMaker.kings.get(pl).name();
            			if(teamName.equals("BLUE")) teamName = "Blau"; else teamName = "Rot";
        				for(Player p : GameMaker.players.keySet()) p.sendMessage(Kingdoms.prefix+ChatColor.GRAY+"Team "+teamName+" hat noch "+lives+" Leben!");
        			}
        		} else {
        			pl.setMetadata("kinglives", new FixedMetadataValue(Kingdoms.plugin, 1));
        			String teamName = GameMaker.kings.get(pl).name();
        			if(teamName.equals("BLUE")) teamName = "Blau"; else teamName = "Rot";
        			for(Player p : GameMaker.players.keySet()) p.sendMessage(Kingdoms.prefix+ChatColor.GRAY+"Team "+teamName+" hat noch 2 Leben!");
        			if(GameMaker.kings.get(pl).equals(Team.RED))ScoreboardManager.updateScoreboard(2, 5); else
        				ScoreboardManager.updateScoreboard(5, 2);
        		}
        	}
        	
        	if(GameMaker.players.containsKey(pl)){
        		if(GameMaker.players.get(pl).equals(Team.RED) && !GameMaker.kings.containsKey(pl)) GameMaker.redCounter--; else if(GameMaker.players.get(pl).equals(Team.BLUE) && !GameMaker.kings.containsKey(pl)) GameMaker.blueCounter--;
        		
        		if((GameMaker.players.get(pl).equals(Team.RED) && ScoreboardManager.getRedLives() == 0) || (GameMaker.players.get(pl).equals(Team.BLUE) && ScoreboardManager.getBlueLives() == 0)){
        			pl.setGameMode(GameMode.SPECTATOR);
        			if(GameMaker.kings.containsKey(pl)){
        				pl.sendMessage(ChatColor.RED+"Du hast keine Leben mehr!"); 
        				pl.getWorld().spawnEntity(pl.getLocation(), EntityType.FIREWORK);
    				} else {
        				pl.sendMessage(ChatColor.RED+"Schade! Dein K�nig ist gestorben, deswegen konntest du nicht respawnen.");
    				}
        		}
        		
        		if(GameMaker.redCounter <= 0 || GameMaker.blueCounter <= 0){
        			for(Player p : GameMaker.players.keySet()){
        				String winnerTeam;
        				if(GameMaker.redCounter <= 0) winnerTeam = "Blau"; else winnerTeam = "Rot";
        				p.sendMessage(ChatColor.AQUA+"Das Team "+winnerTeam+" hat gewonnen!\n"+ChatColor.GRAY+"Teleport zur Lobby in 3 Sekunden...");
        				Bukkit.getServer().getScheduler().runTaskLaterAsynchronously(Kingdoms.plugin, new Runnable() {
        					public void run() {
    							p.teleport(new Location(Bukkit.getWorld(GameMaker.WORLD), Kingdoms.plugin.getConfig().getDouble("spawn_loc_lobby_x"), Kingdoms.plugin.getConfig().getDouble("spawn_loc_lobby_y"), Kingdoms.plugin.getConfig().getDouble("spawn_loc_lobby_z")));
    							p.setBedSpawnLocation(new Location(Bukkit.getWorld(GameMaker.WORLD), Kingdoms.plugin.getConfig().getDouble("spawn_loc_lobby_x"), Kingdoms.plugin.getConfig().getDouble("spawn_loc_lobby_y"), Kingdoms.plugin.getConfig().getDouble("spawn_loc_lobby_z")));
        					}
        				}, 60L);
        			}
        			
        		GameMaker.resetGame();
        		}
        	}
        }
    }
}
