package me.glumandala.kingdoms.events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import me.glumandala.kingdoms.GameMaker;

public class MoveEvent implements Listener {
	
	@EventHandler
    public void onPlayerDeath(PlayerMoveEvent event)  {
		if(GameMaker.freeze == true && GameMaker.players.containsKey(event.getPlayer())) event.setCancelled(true);
	}
}
