package me.glumandala.kingdoms.commands;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.glumandala.kingdoms.Kingdoms;

public class spawnpoint implements CommandExecutor {
	public spawnpoint(Kingdoms plugin) {}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(sender instanceof Player){
			Player pl = (Player)sender;
			if(pl.hasPermission("spawnpoint")){
				if(args.length < 2){
					sender.sendMessage(ChatColor.RED+"/spawnpoint <Kit> <Blue/Red>");
					sender.sendMessage(ChatColor.RED+"             ---");
					
					StringBuffer ritter = new StringBuffer(ChatColor.GRAY+"Ritter: ");
					boolean ritter_red = Kingdoms.plugin.getConfig().isSet("spawn_loc_ritter_rot_x");
					boolean ritter_blue = Kingdoms.plugin.getConfig().isSet("spawn_loc_ritter_blau_x");
					if(ritter_red) ritter.append(ChatColor.GREEN+"ROT "); else ritter.append(ChatColor.RED+"ROT ");
					if(ritter_blue) ritter.append(ChatColor.GREEN+"BLAU"); else ritter.append(ChatColor.RED+"BLAU");
					
					StringBuffer koenig = new StringBuffer(ChatColor.GRAY+"K�nig: ");
					boolean koenig_red = Kingdoms.plugin.getConfig().isSet("spawn_loc_koenig_rot_x");
					boolean koenig_blue = Kingdoms.plugin.getConfig().isSet("spawn_loc_koenig_blau_x");
					if(koenig_red) koenig.append(ChatColor.GREEN+"ROT "); else koenig.append(ChatColor.RED+"ROT ");
					if(koenig_blue) koenig.append(ChatColor.GREEN+"BLAU"); else koenig.append(ChatColor.RED+"BLAU");
					
					StringBuffer hexe = new StringBuffer(ChatColor.GRAY+"Hexe: ");
					boolean hexe_red = Kingdoms.plugin.getConfig().isSet("spawn_loc_hexe_rot_x");
					boolean hexe_blue = Kingdoms.plugin.getConfig().isSet("spawn_loc_hexe_blau_x");
					if(hexe_red) hexe.append(ChatColor.GREEN+"ROT "); else hexe.append(ChatColor.RED+"ROT ");
					if(hexe_blue) hexe.append(ChatColor.GREEN+"BLAU"); else hexe.append(ChatColor.RED+"BLAU");
					
					StringBuffer kerkermeister = new StringBuffer(ChatColor.GRAY+"Kerkermeister: ");
					boolean kerkermeister_red = Kingdoms.plugin.getConfig().isSet("spawn_loc_kerkermeister_rot_x");
					boolean kerkermeister_blue = Kingdoms.plugin.getConfig().isSet("spawn_loc_kerkermeister_blau_x");
					if(kerkermeister_red) kerkermeister.append(ChatColor.GREEN+"ROT "); else kerkermeister.append(ChatColor.RED+"ROT ");
					if(kerkermeister_blue) kerkermeister.append(ChatColor.GREEN+"BLAU"); else kerkermeister.append(ChatColor.RED+"BLAU");

					StringBuffer frontenkaempfer = new StringBuffer(ChatColor.GRAY+"Frontenk�mpfer: ");
					boolean frontenkaempfer_red = Kingdoms.plugin.getConfig().isSet("spawn_loc_frontenk�mpfer_rot_x");
					boolean frontenkaempfer_blue = Kingdoms.plugin.getConfig().isSet("spawn_loc_frontenk�mpfer_blau_x");
					if(frontenkaempfer_red) frontenkaempfer.append(ChatColor.GREEN+"ROT "); else frontenkaempfer.append(ChatColor.RED+"ROT ");
					if(frontenkaempfer_blue) frontenkaempfer.append(ChatColor.GREEN+"BLAU"); else frontenkaempfer.append(ChatColor.RED+"BLAU");
					
					StringBuffer bogenschuetze = new StringBuffer(ChatColor.GRAY+"Bogensch�tze: ");
					boolean bogenschuetze_red = Kingdoms.plugin.getConfig().isSet("spawn_loc_bogensch�tze_rot_x");
					boolean bogenschuetze_blue = Kingdoms.plugin.getConfig().isSet("spawn_loc_bogensch�tze_blau_x");
					if(bogenschuetze_red) bogenschuetze.append(ChatColor.GREEN+"ROT "); else bogenschuetze.append(ChatColor.RED+"ROT ");
					if(bogenschuetze_blue) bogenschuetze.append(ChatColor.GREEN+"BLAU"); else bogenschuetze.append(ChatColor.RED+"BLAU");
					
					StringBuffer heiler = new StringBuffer(ChatColor.GRAY+"Heiler: ");
					boolean heiler_red = Kingdoms.plugin.getConfig().isSet("spawn_loc_heiler_rot_x");
					boolean heiler_blue = Kingdoms.plugin.getConfig().isSet("spawn_loc_heiler_blau_x");
					if(heiler_red) heiler.append(ChatColor.GREEN+"ROT "); else heiler.append(ChatColor.RED+"ROT ");
					if(heiler_blue) heiler.append(ChatColor.GREEN+"BLAU"); else heiler.append(ChatColor.RED+"BLAU");
					
					StringBuffer gameoverString = new StringBuffer(ChatColor.GRAY+"Gameover: ");
					boolean gameover = Kingdoms.plugin.getConfig().isSet("spawn_loc_gameover_x");
					if(gameover) gameoverString.append(ChatColor.GREEN+"JA "); else gameoverString.append(ChatColor.RED+"NEIN ");
					
					StringBuffer lobbyString = new StringBuffer(ChatColor.GRAY+"Lobby: ");
					boolean lobby = Kingdoms.plugin.getConfig().isSet("spawn_loc_lobby_x");
					if(lobby) lobbyString.append(ChatColor.GREEN+"JA "); else lobbyString.append(ChatColor.RED+"NEIN ");
					
					sender.sendMessage(String.valueOf(ritter));
					sender.sendMessage(String.valueOf(hexe));
					sender.sendMessage(String.valueOf(kerkermeister));
					sender.sendMessage(String.valueOf(frontenkaempfer));
					sender.sendMessage(String.valueOf(bogenschuetze));
					sender.sendMessage(String.valueOf(heiler));
					sender.sendMessage(String.valueOf(gameoverString));
					sender.sendMessage(String.valueOf(lobbyString));
					sender.sendMessage(String.valueOf(koenig));
				} else {
					String kit = args[0];
					String kitNoLower = kit;
					 kit = kit.toLowerCase();
					String team = args[1];
					Location loc = pl.getLocation();
					
					if(team.equalsIgnoreCase("blau")){
						setLocations(kit, loc, kitNoLower, team, pl);
						Kingdoms.plugin.saveConfig();
					} else if(team.equalsIgnoreCase("rot")){
						setLocations(kit, loc, kitNoLower, team, pl);
						Kingdoms.plugin.saveConfig();
					} else {
						pl.sendMessage(ChatColor.RED+"Unbekanntes Team: "+team);
					}
				}
			} else {
				pl.sendMessage(Kingdoms.prefix+ChatColor.RED+"Du hast keine Rechte dazu.");
			}
		} else {
			sender.sendMessage("Dieser Befehl kann nur im Spiel benutzt werden.");
		}
		
		return true;
	}
	
	private static void setLocations(String kit, Location loc, String kitNoLower, String team, Player pl){
		switch(kit){
		case "ritter": 
			Kingdoms.plugin.getConfig().set("spawn_loc_"+kit+"_"+team+"_x", loc.getX());
			Kingdoms.plugin.getConfig().set("spawn_loc_"+kit+"_"+team+"_y", loc.getY());
			Kingdoms.plugin.getConfig().set("spawn_loc_"+kit+"_"+team+"_z", loc.getZ());
			pl.sendMessage(ChatColor.GREEN+"Spawnlocation f�r Team "+team+", Kit "+kitNoLower+" wurde gesetzt.");
			break;
			
		case "hexe": 
			Kingdoms.plugin.getConfig().set("spawn_loc_"+kit+"_"+team+"_x", loc.getX());
			Kingdoms.plugin.getConfig().set("spawn_loc_"+kit+"_"+team+"_y", loc.getY());
			Kingdoms.plugin.getConfig().set("spawn_loc_"+kit+"_"+team+"_z", loc.getZ());
			pl.sendMessage(ChatColor.GREEN+"Spawnlocation f�r Team "+team+", Kit "+kitNoLower+" wurde gesetzt.");
			break;
			
		case "kerkermeister": 
			Kingdoms.plugin.getConfig().set("spawn_loc_"+kit+"_"+team+"_x", loc.getX());
			Kingdoms.plugin.getConfig().set("spawn_loc_"+kit+"_"+team+"_y", loc.getY());
			Kingdoms.plugin.getConfig().set("spawn_loc_"+kit+"_"+team+"_z", loc.getZ());
			pl.sendMessage(ChatColor.GREEN+"Spawnlocation f�r Team "+team+", Kit "+kitNoLower+" wurde gesetzt.");
			break;
			
		case "frontenk�mpfer": 
			Kingdoms.plugin.getConfig().set("spawn_loc_"+kit+"_"+team+"_x", loc.getX());
			Kingdoms.plugin.getConfig().set("spawn_loc_"+kit+"_"+team+"_y", loc.getY());
			Kingdoms.plugin.getConfig().set("spawn_loc_"+kit+"_"+team+"_z", loc.getZ());
			pl.sendMessage(ChatColor.GREEN+"Spawnlocation f�r Team "+team+", Kit "+kitNoLower+" wurde gesetzt.");
			break;
			
		case "bogensch�tze": 
			Kingdoms.plugin.getConfig().set("spawn_loc_"+kit+"_"+team+"_x", loc.getX());
			Kingdoms.plugin.getConfig().set("spawn_loc_"+kit+"_"+team+"_y", loc.getY());
			Kingdoms.plugin.getConfig().set("spawn_loc_"+kit+"_"+team+"_z", loc.getZ());
			pl.sendMessage(ChatColor.GREEN+"Spawnlocation f�r Team "+team+", Kit "+kitNoLower+" wurde gesetzt.");
			break;
			
		case "heiler": 
			Kingdoms.plugin.getConfig().set("spawn_loc_"+kit+"_"+team+"_x", loc.getX());
			Kingdoms.plugin.getConfig().set("spawn_loc_"+kit+"_"+team+"_y", loc.getY());
			Kingdoms.plugin.getConfig().set("spawn_loc_"+kit+"_"+team+"_z", loc.getZ());
			pl.sendMessage(ChatColor.GREEN+"Spawnlocation f�r Team "+team+", Kit "+kitNoLower+" wurde gesetzt.");
			break;
			
		case "k�nig": 
			Kingdoms.plugin.getConfig().set("spawn_loc_"+kit+"_"+team+"_x", loc.getX());
			Kingdoms.plugin.getConfig().set("spawn_loc_"+kit+"_"+team+"_y", loc.getY());
			Kingdoms.plugin.getConfig().set("spawn_loc_"+kit+"_"+team+"_z", loc.getZ());
			pl.sendMessage(ChatColor.GREEN+"Spawnlocation f�r Team "+team+", Kit "+kitNoLower+" wurde gesetzt.");
			break;
			
		case "gameover": 
			Kingdoms.plugin.getConfig().set("spawn_loc_gameover_x", loc.getX());
			Kingdoms.plugin.getConfig().set("spawn_loc_gameover_y", loc.getY());
			Kingdoms.plugin.getConfig().set("spawn_loc_gameover_z", loc.getZ());
			pl.sendMessage(ChatColor.GREEN+"Spawnlocation f�r ein Gameover wurde gesetzt.");
			break;
			
		case "lobby": 
			Kingdoms.plugin.getConfig().set("spawn_loc_lobby_x", loc.getX());
			Kingdoms.plugin.getConfig().set("spawn_loc_lobby_y", loc.getY());
			Kingdoms.plugin.getConfig().set("spawn_loc_lobby_z", loc.getZ());
			pl.sendMessage(ChatColor.GREEN+"Spawnlocation f�r die Lobby wurde gesetzt.");
			break;
		
		default: 
			pl.sendMessage(ChatColor.RED+"Unbekanntes Kit: "+kitNoLower);
			break;
	}
	}
}
