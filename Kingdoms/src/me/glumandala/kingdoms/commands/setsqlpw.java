package me.glumandala.kingdoms.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.glumandala.kingdoms.Kingdoms;

public class setsqlpw implements CommandExecutor {
	public setsqlpw(Kingdoms plugin) {}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(sender instanceof Player){
			Player pl = (Player)sender;
			if(pl.hasPermission("setsqlpw")){
				if(args.length > 0){
					Kingdoms.plugin.getConfig().set("SWL_PASSWORD", args[0]);
					Kingdoms.plugin.saveConfig();
					pl.sendMessage(ChatColor.GREEN+"Das SQL-Passwort wurde gesetzt.");
				} else {
					pl.sendMessage(Kingdoms.prefix+ChatColor.RED+"/setsqlpw <Passwort>");
				}
			} else {
				pl.sendMessage(Kingdoms.prefix+ChatColor.RED+"Du hast keine Rechte dazu.");
			}
		} else {
			sender.sendMessage("Dieser Befehl kann nur im Spiel benutzt werden.");
		}
		
		return true;
	}
}
