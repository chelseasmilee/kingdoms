package me.glumandala.kingdoms.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.glumandala.kingdoms.GameMaker;
import me.glumandala.kingdoms.Kingdoms;

public class join implements CommandExecutor {

	public join(Kingdoms plugin) {}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(sender instanceof Player){
			Player pl = (Player)sender;
			GameMaker.addPlayer(pl);
		} else {
			sender.sendMessage("Dieser Befehl kann nur im Spiel benutzt werden.");
		}
		
		return true;
	}
}
