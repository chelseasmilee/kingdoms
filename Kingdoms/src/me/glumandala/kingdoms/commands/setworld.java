package me.glumandala.kingdoms.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.glumandala.kingdoms.Kingdoms;

public class setworld implements CommandExecutor {
	public setworld(Kingdoms plugin) {}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(sender instanceof Player){
			Player pl = (Player)sender;
			if(pl.hasPermission("setworld")){
				if(args.length > 0){
					Kingdoms.plugin.getConfig().set("MAIN_WORLD", args[0]);
					Kingdoms.plugin.saveConfig();
					pl.sendMessage(ChatColor.GREEN+"Die Spielwelt wurde gesetzt.");
				} else {
					pl.sendMessage(Kingdoms.prefix+ChatColor.RED+"/setworld <Welt>");
				}
			} else {
				pl.sendMessage(Kingdoms.prefix+ChatColor.RED+"Du hast keine Rechte dazu.");
			}
		} else {
			sender.sendMessage("Dieser Befehl kann nur im Spiel benutzt werden.");
		}
		
		return true;
	}
}
