package me.glumandala.kingdoms.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.glumandala.kingdoms.GameMaker;
import me.glumandala.kingdoms.Kingdoms;

public class leave implements CommandExecutor {
	public leave(Kingdoms plugin) {}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(sender instanceof Player){
			Player pl = (Player)sender;
			if(GameMaker.players.containsKey(pl)){
				GameMaker.removePlayer(pl);
				pl.sendMessage(ChatColor.GREEN+"Du hast dich aus der Warteschlange ausgetragen.");
			} else {
				pl.sendMessage(ChatColor.RED+"Du bist dem Spiel bisher nicht beigetreten.");
			}
		} else {
			sender.sendMessage("Dieser Befehl kann nur im Spiel benutzt werden.");
		}
		return true;
	}
}
